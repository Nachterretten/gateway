package publisher

import (
	"context"
	"fmt"
	"gitlab.com/Nachterretten/json-rpc/config"
	"go.uber.org/zap"

	tgbot "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"sync"
)

type Telegram struct {
	conf   config.Telegram
	logger *zap.Logger
	bot    *tgbot.BotAPI
	sync.RWMutex
}

func NewTelegram(conf config.Telegram, logger *zap.Logger) *Telegram {
	bot, err := tgbot.NewBotAPI(conf.Token)
	if err != nil {
		logger.Fatal(fmt.Sprintf("publisher not connected: %s", err))
	}
	logger.Info("publisher connected")
	return &Telegram{conf: conf, logger: logger, bot: bot}
}

func (t *Telegram) updateTelegram() int64 {
	u := tgbot.NewUpdate(0)
	u.Timeout = 10

	updates := t.bot.GetUpdatesChan(u)
	for r := range updates {
		if r.Message == nil {
			continue
		}
		id := r.Message.Chat.ID
		return id
	}
	return 0
}

func (t *Telegram) Run(ctx context.Context, logger *zap.Logger) error {
	return t.rabbitConnect(logger, ctx)
}
