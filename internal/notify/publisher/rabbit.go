package publisher

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type Rabbit struct {
	logger      *zap.Logger
	amqpChannel *amqp091.Channel
}

func (t *Telegram) rabbitConnect(logger *zap.Logger, ctx context.Context) error {
	//Установка соединения с RabbitMQ
	conn, err := amqp091.Dial("amqp://guest:guest@rabbitmq:5672/") // localhost
	if err != nil {
		logger.Error("connection error", zap.Error(err))
		return err
	}
	defer conn.Close()
	//Установление канала(протокол связи по соединению)
	amqpChannel, err := conn.Channel()
	if err != nil {
		logger.Error("channel error", zap.Error(err))
		return err
	}

	defer amqpChannel.Close()

	//сообщение серверу об интересующей нас задаче
	//Add - название очереди
	queue, err := amqpChannel.QueueDeclare("adder", true, false, false, false, nil)
	if err != nil {
		logger.Error("queue error ", zap.Error(err))

	}
	//Параметры чтобы очереди выполнялись последовательно
	err = amqpChannel.Qos(1, 0, false)
	if err != nil {
		logger.Error("qos error", zap.Error(err))

	}
	messageChannel, err := amqpChannel.Consume(queue.Name, "", true, false, false, false, nil)
	//Канал чтобы горутина работала в фоновом режиме
	stopChan := make(chan bool)
	//получаем id от бота
	id := t.updateTelegram()

	go func() {
		for message := range messageChannel {
			t.RWMutex.Lock()
			logger.Info(fmt.Sprintf("recieve a body: %s", message.Body))

			//получаем сообщение от Rabbit и отправляем в бота
			_, err = t.bot.Send(tgbotapi.NewMessage(id, string(message.Body)))
			if err != nil {
				logger.Error("send error", zap.Error(err))
			}
			t.RWMutex.Unlock()
		}
	}()
	select {
	case <-stopChan:
		logger.Info("close chanel")
		return err
	case <-ctx.Done():
	}
	return nil
}
