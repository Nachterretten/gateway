package docs

import "gitlab.com/Nachterretten/json-rpc/internal/modules/worker/controllers"

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/worker/history worker workerHistoryRequest

// swagger:route GET /api/1/worker/history worker workerHistoryRequest
// Получение истории заказов.
// security:
//   - Bearer: []
// responses:
//   200: workerHistoryResponse

// swagger:response workerHistoryResponse
//
//nolint:all
type workerHistoryResponse struct {
	//in:body
	Body []controllers.ProfileResponseHistory
}

// swagger:route GET /api/1/worker/max worker workerMaxRequest
// Получение валют с максимальной цены.
// security:
//   - Bearer: []
// responses:
//   200: workerMaxResponse

// swagger:response workerMaxResponse
//
//nolint:all
type workerMaxResponse struct {
	//in:body
	Body []controllers.ProfileResponseMax
}

// swagger:route GET /api/1/worker/min worker workerMinRequest
// Получение валют с минимальной цены.
// security:
//   - Bearer: []
// responses:
//   200: workerMinResponse

// swagger:response workerMinResponse
//
//nolint:all
type workerMinResponse struct {
	//in:body
	Body []controllers.ProfileResponseMin
}

// swagger:route GET /api/1/worker/avg worker workerAvgRequest
// Получение списка пар со средней ценой.
// security:
//   - Bearer: []
// responses:
//   200: workerAvgResponse

// swagger:response workerAvgResponse
//
//nolint:all
type workerAvgResponse struct {
	//in:body
	Body []controllers.ProfileResponseAvg
}
