package modules

import (
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/client"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	aservice "gitlab.com/Nachterretten/json-rpc/internal/modules/auth/service"
	uservice "gitlab.com/Nachterretten/json-rpc/internal/modules/user/service"
	wservice "gitlab.com/Nachterretten/json-rpc/internal/modules/worker/service"
	"gitlab.com/Nachterretten/json-rpc/rpc/proto_auth"
	"gitlab.com/Nachterretten/json-rpc/rpc/proto_user"
	"gitlab.com/Nachterretten/json-rpc/rpc/proto_worker"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
	Work wservice.Workerer
}

func NewServicesJRPC(components *component.Components) *Services {
	var authClient client.Client
	var userClient client.Client
	var workClient client.Client

	authClient = client.NewJSONRPC(components.Conf.AuthRPC, components.Logger)
	userClient = client.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	workClient = client.NewJSONRPC(components.Conf.WorkerRPC, components.Logger)

	authClientRPC := aservice.NewAuthServiceJSONRPC(authClient)
	userClientRPC := uservice.NewUserServiceJSONRPC(userClient)
	workClientRPC := wservice.NewWorkServiceJRPC(workClient)
	return &Services{
		User: userClientRPC,
		Auth: authClientRPC,
		Work: workClientRPC,
	}
}

func NewServiceGRPC(components *component.Components) *Services {
	authGRPC := client.NewGRPC(components.Conf.AuthRPC, components.Logger)
	authClient := proto_auth.NewAuthServiceRPCClient(authGRPC)
	authService := aservice.NewExchangeServiceGRPC(authClient)

	workerGRPC := client.NewGRPC(components.Conf.WorkerRPC, components.Logger)
	workerClient := proto_worker.NewExchangeServiceRPCClient(workerGRPC)
	workerService := wservice.NewExchangeServiceGRPC(workerClient)

	userGRPC := client.NewGRPC(components.Conf.UserRPC, components.Logger)
	userClient := proto_user.NewUserServiceRPCClient(userGRPC)
	userService := uservice.NewExchangeServiceGRPC(userClient)

	return &Services{
		Work: workerService,
		Auth: authService,
		User: userService,
	}
}
