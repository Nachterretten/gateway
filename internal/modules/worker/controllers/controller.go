package controllers

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/component"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/responder"
	"gitlab.com/Nachterretten/json-rpc/internal/modules/worker/service"
	"net/http"
)

type Workerer interface {
	MinPrice(w http.ResponseWriter, r *http.Request)
	MaxPrices(w http.ResponseWriter, r *http.Request)
	AveragePrices(w http.ResponseWriter, r *http.Request)
	History(w http.ResponseWriter, r *http.Request)
}

type Worker struct {
	service service.Workerer
	responder.Responder
	godecoder.Decoder
}

func (worker *Worker) History(w http.ResponseWriter, r *http.Request) {
	res := worker.service.History(r.Context())
	if res.ErrorCode != errors.NoError {
		worker.OutputJSON(w, ProfileResponseHistory{
			ErrorCode: res.ErrorCode,
			Data:      DataHistory{Message: "history work error"},
		})
		return
	}
	worker.OutputJSON(w, ProfileResponseHistory{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataHistory{Worker: res.History},
	})
}

func (worker *Worker) MinPrice(w http.ResponseWriter, r *http.Request) {
	res := worker.service.MinPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		worker.OutputJSON(w, ProfileResponseMin{
			ErrorCode: res.ErrorCode,
			Data:      DataMin{Message: "min work error"},
		})
		return
	}
	worker.OutputJSON(w, ProfileResponseMin{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataMin{Worker: res.Worker},
	})
}

func (worker *Worker) MaxPrices(w http.ResponseWriter, r *http.Request) {
	res := worker.service.MaxPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		worker.OutputJSON(w, ProfileResponseMax{
			ErrorCode: res.ErrorCode,
			Data:      DataMax{Message: "max work error"},
		})
		return
	}
	worker.OutputJSON(w, ProfileResponseMax{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataMax{Worker: res.Worker},
	})
}

func (worker *Worker) AveragePrices(w http.ResponseWriter, r *http.Request) {
	res := worker.service.AvgPrices(r.Context())
	if res.ErrorCode != errors.NoError {
		worker.OutputJSON(w, ProfileResponseAvg{
			ErrorCode: res.ErrorCode,
			Data:      DataAvg{Message: "no work avg"},
		})
		return
	}
	worker.OutputJSON(w, ProfileResponseAvg{
		Success:   true,
		ErrorCode: res.ErrorCode,
		Data:      DataAvg{Worker: res.Worker},
	})
}

func NewWorker(service service.Workerer, components *component.Components) Workerer {
	return &Worker{service: service, Responder: components.Responder, Decoder: components.Decoder}
}
