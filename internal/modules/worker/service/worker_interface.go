package service

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/internal/models"
)

type Workerer interface {
	MinPrices(ctx context.Context) WorkerMinPriceOut
	MaxPrices(ctx context.Context) WorkerMaxPriceOut
	AvgPrices(ctx context.Context) WorkerAvgPriceOut
	History(ctx context.Context) WorkerHistory
}

type WorkerMinPriceOut struct {
	Worker    []models.WorkerMinPrice
	ErrorCode int
	Succses   bool
}

type WorkerMaxPriceOut struct {
	Worker    []models.WorkerMaxPrice
	ErrorCode int
	Succses   bool
}

type WorkerAvgPriceOut struct {
	Worker    []models.WorkerAvgPrice
	ErrorCode int
	Succses   bool
}

type WorkerHistory struct {
	History   []models.WorkerDTO
	ErrorCode int
	Succses   bool
}
