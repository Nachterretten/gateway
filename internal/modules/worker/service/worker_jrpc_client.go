package service

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/client"
	"log"
)

type WorkerServerJGRPC struct {
	client client.Client
}

var Empty struct{}

func (w *WorkerServerJGRPC) MinPrices(ctx context.Context) WorkerMinPriceOut {
	var out WorkerMinPriceOut
	err := w.client.Call("WorkerServerJRPC.MinPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJGRPC) MaxPrices(ctx context.Context) WorkerMaxPriceOut {
	var out WorkerMaxPriceOut
	err := w.client.Call("WorkerServerJRPC.MaxPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJGRPC) AvgPrices(ctx context.Context) WorkerAvgPriceOut {
	var out WorkerAvgPriceOut
	err := w.client.Call("WorkerServerJRPC.AvgPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJGRPC) History(ctx context.Context) WorkerHistory {
	var out WorkerHistory
	err := w.client.Call("WorkerServerJRPC.History", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func NewWorkServiceJRPC(client client.Client) *WorkerServerJGRPC {
	return &WorkerServerJGRPC{client: client}
}
