package service

import (
	"context"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/client"
	"gitlab.com/Nachterretten/json-rpc/internal/infrastructure/errors"
)

type AuthServiceJSONRPC struct {
	client client.Client
}

func NewAuthServiceJSONRPC(client client.Client) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u

}

func (a *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	var out RegisterOut
	err := a.client.Call("AuthServiceJSONRPC.Register", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out
}

func (a *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := a.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out
}

func (a *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := a.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}
	return out

}
